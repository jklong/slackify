FROM python:alpine
RUN apk add build-base libffi-dev libressl-dev

#Putting this on its own layer to speed up rebuilds
RUN pip install cryptography 
COPY dist /
RUN pip install slackify-1.0.0-py3-none-any.whl

VOLUME [ "/data" ]
WORKDIR /data

ENTRYPOINT [ "python", "-m", "slackify" ]
