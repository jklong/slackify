import getpass
import json
import os
import platform
from base64 import b64decode

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, padding
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

iv = b'\x86\xc5\xf2\xefp\xc9\xe9\x8a\x10K\xa1FH\xc9\xd2<'

def pad(data, pad_len):
    padder = padding.PKCS7(128).padder()
    padded_data = padder.update(bytes(data, 'utf-8')) + padder.finalize()
    return padded_data

def unpad(data):
    unpadder = padding.PKCS7(128).unpadder()
    unpadded_data = unpadder.update(data) + unpadder.finalize()
    return unpadded_data

def make_user_key(key_len):
   '''
   Generates a key that is unique to a user on a machine.
   '''

   kdf_salt = b'fX\xb8\x17\xbeW\x18\xa8Fa\x88WWn/S'
   salt = "76e5f5d6f8a62e410a384676ed2f78331f6791db211f227048a11b71e3a1a27c" 
   
   username = getpass.getuser()

   if 'NAME' not in os.environ: 
        machine = platform.node()
   else:
        machine = os.environ['NAME']

   id_string = username + salt + machine

   kdf = PBKDF2HMAC(
       algorithm = hashes.SHA256(),
       length = key_len,
       salt = kdf_salt,
       iterations = 50000,
       backend = default_backend()
   )

   key = kdf.derive(bytes(id_string, "utf-8"))
   return key

def write_config(config, file_path):
    conf_text = pad(json.dumps(config), 16)
    key = make_user_key(32)

    cipher = Cipher(algorithms.AES(key), modes.CBC(iv), default_backend())  
    enc = cipher.encryptor()

    ciphertext = enc.update(conf_text) + enc.finalize()

    with open(file_path, 'wb') as f:
        f.write(ciphertext)
   
def load_config(file_path):
    ciphertext = b''
    with open(file_path, 'rb') as f:
        ciphertext = f.read()
    
    key = make_user_key(32)
    cipher = Cipher(algorithms.AES(key), modes.CBC(iv), default_backend())  
    dec = cipher.decryptor()

    conf_json = unpad(dec.update(ciphertext) + dec.finalize())
    return json.loads(conf_json)

def load_blob(blob, password):
    blob = b64decode(blob)
    kdf = PBKDF2HMAC(algorithm = hashes.SHA256(),
                        length = 32, 
                        salt = b'dickdickdickdick', 
                        iterations= 50000, 
                        backend=default_backend())
    key = kdf.derive(bytes(password, 'utf-8'))
    cipher = Cipher(algorithms.AES(key), modes.CBC(b'dickdickdickdick'), default_backend())
    dec = cipher.decryptor()
    return str(unpad(dec.update(blob) + dec.finalize()), 'utf-8')
