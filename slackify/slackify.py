import getpass
import json
import os
import re
import sys
from datetime import datetime, timedelta
from time import sleep

import click
import requests
import slack

#import util
from . import util

CONFIG = 'conf.slackify'
DEFAULT_CHANNEL = 'music'
WATCH_INTERVAL = 1 #Interval for slack refreshes, in seconds

SLACK_CLIENT_ID = ""
SLACK_CLIENT_ID_BLOB = "FZ7T4g0SdCPWbXvWwV0QPx2bheDkaRL3usWlSjLCacg="
SLACK_SECRET = ""
SLACK_SECRET_BLOB = "g1vgNHeFWVIwIGs2HXUqrH51gICyej7SbEdQBneFKcTiZWCiHn9OpN7ivsY+Lff6"
SPOTIFY_CLIENT_ID = ""
SPOTIFY_CLIENT_ID_BLOB = "TDvuxmpNMmgiu3UNK3awlhCzmoaHJz192Aaj6J5k/gPzsoISKQUrahkS7HEd2BiU"
SPOTIFY_SECRET = ""
SPOTIFY_SECRET_BLOB = "WYiNYSgX4XKQ32c5trQzac6o1svkhLA9GYqRcrUmHzVpUZIHrk/0JyfTjHtBUx8I"

SLACK_SCOPES = "identify,channels:history,channels:read,chat:write"
SLACK_TOKEN_URL = "https://slack.com/api/oauth.v2.access"

SPOTIFY_SCOPES="user-modify-playback-state user-read-playback-state"
SPOTIFY_QUEUE_ENDPOINT="https://api.spotify.com/v1/me/player/queue"
SPOTIFY_PLAYING_ENDPOINT = "https://api.spotify.com/v1/me/player/currently-playing"

config = {}
track_regex = re.compile(r'.*https:\/\/open.spotify.com\/(track|album)\/([0-9a-zA-Z_]*).*?[\|\>]?\>.*')

LINK_TEMPLATE = "<{0}|{0}>"

class SlackWrapper:

    def __init__(self):
        self.__client = slack.WebClient(token=config['slack']['api_token'])

    def channel_id(self, channel):
        '''
        Find a channel by name and return the id
        '''
        channel_list = self.__client.conversations_list().data

        if not 'ok' in channel_list.keys() or not channel_list['ok']:
            print("! [Slack] Couldn't fetch channel id")
        
        for c in channel_list['channels']:
            if c["name"] == channel:
                return c["id"]
        
        if config['debug']:
            print("! [Slack] Channel {} not found".format(channel))
        
        return None

    def get_channel_messages(self):
        '''
        Get the recent messages from the channel
        '''
        chan_id = config['slack']['channel_id']
        # TODO: limit to the last timestamp
        return self.__client.conversations_history(channel=chan_id).data
    
    def get_replies(self, ts):
        '''
        Get the thread at timestamp ts. 
        '''
        chan_id = config['slack']['channel_id']
        thread = self.__client.conversations_replies(channel=chan_id, ts=ts).data
        if not thread['ok']:
            return None
        else:
            return thread['messages']
    
    def post_link(self, link):
        print ("* [Slack] Sending link {}".format(link))
        msg = LINK_TEMPLATE.format(link)
        response = self.__client.chat_postMessage(channel=config['slack']['channel_id'], text=msg).data

        if not response['ok']:
            print("* [Slack] Error posting message")

            if config['debug']:
                print(response)

class SpotifyWrapper:
    def add_track_at_ts(self, ts):
        '''
        Add a track defined at a slack timestamp
        '''
        message = slack_client.get_replies(ts)[0]
        matches = track_regex.match(message['text'])

        if matches != None:
            self.add_track(matches.group(1), matches.group(2))
        elif config['debug']:
            print("* [Spotify] Timestamp doesn't have a track")

    def refresh(self):
        '''
        Refreshes the token and saves it to the config
        '''
        if config['debug']:
            print("* Refreshing api token with refresh token: {}".format(config['spotify']['refresh']))

        postdata = {'grant_type': 'refresh_token', 'refresh_token': config['spotify']['refresh']}
        req = requests.post('https://accounts.spotify.com/api/token', postdata, auth=(SPOTIFY_CLIENT_ID, SPOTIFY_SECRET))

        req_dict = json.loads(req.text)

        if req.status_code != 200:
            print("! Error refreshing token")
            if config['debug']:
                print(req.text)
        else:
            config['spotify']['api_token'] = req_dict['access_token']
            config['spotify']['expiry'] = (datetime.now() + timedelta(seconds=req_dict['expires_in'])).timestamp()
            util.write_config(config, CONFIG)

    def add_track(self, id_type, id):
        '''
        Add a track to the spotify user's queue
        '''
        if config['debug']:
            print("* [Spotify] Adding {} with id {}".format(id_type, id))
        
        if datetime.now().timestamp() > config['spotify']['expiry']:
            self.refresh()

        headers={'Authorization': "Bearer {}".format(config['spotify']['api_token'])}
        uri = "spotify:{}:{}".format(id_type, id)
        if config['debug']:
            print("* Track URI {}".format(uri))
        req = requests.post(SPOTIFY_QUEUE_ENDPOINT + "?uri=" + uri, headers=headers) 

        if req.status_code != 204:
            print("! [Spotify] Error adding track to queue")
        else:
            print("Added {} to queue".format(id)) 

    def now_playing(self):
        '''
        Return a link to the currently playing track
        '''
        if config['debug']:
            print("* Grabbing now playing")
        
        if datetime.now().timestamp() > config['spotify']['expiry']:
            self.refresh()

        headers={'Authorization': "Bearer {}".format(config['spotify']['api_token'])}
        req = requests.get(SPOTIFY_PLAYING_ENDPOINT, headers = headers)

        if req.status_code != 200:
            print("! [Spotify] Error getting now playing")
            if config['debug']:
                print(req.status_code)
                print(req.text)
            return None
        else:
            req_dict = json.loads(req.text)
            return req_dict['item']['external_urls']['spotify']

def wait_for_code():
    '''
    Parse a returned address for a oauth code
    '''

    msg = input("Paste the URL you were redirected to: ")

    return re.match(r'.*?\?code=([a-zA-Z0-9\-_\.]*).*', msg).group(1)

def first_run():
    '''
    This is the first run, generate tokens and save them into our encrypted config
    '''

    # Decrypt the blobs
    global SLACK_CLIENT_ID, SLACK_SECRET, SPOTIFY_CLIENT_ID, SPOTIFY_SECRET
    
    password = getpass.getpass('First run password: ')

    SLACK_CLIENT_ID =   util.load_blob(SLACK_CLIENT_ID_BLOB, password)
    SLACK_SECRET =      util.load_blob(SLACK_SECRET_BLOB, password)
    SPOTIFY_CLIENT_ID = util.load_blob(SPOTIFY_CLIENT_ID_BLOB, password)
    SPOTIFY_SECRET =    util.load_blob(SPOTIFY_SECRET_BLOB, password)


    global config
    config = {'slack': {}, 'spotify':{}}
    token, id =  get_slack_token()
    config['slack']['api_token'] = token
    config['slack']['user_id'] = id

    token, refresh, expiry = get_spotify_token()
    config['spotify']['api_token'] = token
    config['spotify']['refresh'] = refresh
    config['spotify']['expiry'] = expiry

    # If we got this far, the tokens are ok. Write those and our secrets to the config
    config['slack_client'] = SLACK_CLIENT_ID
    config['slack_secret'] = SLACK_SECRET
    config['spotify_client'] = SPOTIFY_CLIENT_ID
    config['spotify_secret'] = SPOTIFY_SECRET

    config['slack']['last_ts'] = datetime.now().timestamp()

    util.write_config(config, CONFIG)

def get_slack_token():
    '''
    Request a token via oauth, authed as our user
    '''
    URL = "https://slack.com/oauth/v2/authorize?response_type=token&user_scope={}&client_id={}".format(SLACK_SCOPES, SLACK_CLIENT_ID )
    print("Go to this to approve the slack sign-in: " + URL)

    code = wait_for_code()
    print("* slack oauth code received")

    postData = {'code': code}
    req = requests.post(SLACK_TOKEN_URL, postData, auth=(SLACK_CLIENT_ID, SLACK_SECRET))

    reqDict = json.loads(req.text)

    if reqDict['ok']:
        print("* slack API Token fetched.")
        token = reqDict['authed_user']['access_token']
        user_id = reqDict['authed_user']['id']
    else:
        print("! Error fetching slack API token")
        print(req.text)
        sys.exit(1)

    return (token, user_id)

def get_spotify_token():
    
    URL = "https://accounts.spotify.com/authorize?response_type=code&client_id={}&scope={}&redirect_uri=http://localhost:8080/".format(SPOTIFY_CLIENT_ID,SPOTIFY_SCOPES)
    print("Go to this one to approve spotify: {}".format(URL))

    code = wait_for_code()
    print("* spotify oauth code received")

    postdata = {'grant_type': 'authorization_code', 'code': code, 'redirect_uri': 'http://localhost:8080/'}
    req = requests.post('https://accounts.spotify.com/api/token', postdata, auth=(SPOTIFY_CLIENT_ID, SPOTIFY_SECRET))

    reqdict = json.loads(req.text)

    if 'error' not in reqdict.keys():
        print("* Got spotify api token")
        token = reqdict['access_token']
        refresh = reqdict['refresh_token']
        expiry = (datetime.now() + timedelta(seconds=reqdict['expires_in'])).timestamp()
    else:
        print("! Error getting spotify api token")
        print(req.text)
        exit(1)
    
    return (token, refresh, expiry)

def watch_messages():
    '''
    Watch a channel ID for messages from the configured user
    '''
    global config

    debug = config['debug']

    print ("* Watching slack for messages")
    if debug:
        print("* Entering slack watch loop")
    
    # Grab the channel history in a loop. Save the time of the last message, the last spotify link.

    while True:
        #Grab the default max of the last 100 messages.
        messages = slack_client.get_channel_messages()
        
        if not messages['ok']:
            print("! Error getting messages")
            if debug:
                print(messages)
        else:
            if debug and len(messages['messages']) > 0:
                print("* Got {} messages".format(len(messages['messages'])))

            message_actions = scan_messages(messages['messages'])

            if message_actions['last_ts'] > 0:
                config['slack']['last_ts'] = message_actions['last_ts']

            # Handle the case of adding the last seen track, then update
            # the last seen timestamp for next iteration

            if message_actions['add_last']:
                if debug:
                    print("* Adding last seen track")

                spotify_client.add_track_at_ts(config['last_spotify_ts'])
            
            if message_actions['last_spotify_ts'] != 0:
                config['slack']['last_spotify_ts'] = message_actions['last_spotify_ts']

            # Add all the seen tracks or timestamps, oldest to newest
            message_actions['spotify_tracks'].reverse()
            for s in message_actions['spotify_tracks']:
                if isinstance(s,tuple):
                    spotify_client.add_track(s[0], s[1])
                else:
                    spotify_client.add_track_at_ts(s)
            
            # Post now playing if we're asked to
            if message_actions['send_link']:
                now_playing = spotify_client.now_playing()
                slack_client.post_link(now_playing)
            
        if debug:
            print("* Sleeping for {} seconds\n".format(WATCH_INTERVAL))
        util.write_config(config, CONFIG)
        sleep(WATCH_INTERVAL)

def scan_messages(message_data):
    '''
    Scan a message dictionary for actions and new spotify links.
    Return a dictionary of info and actions
    '''
    global config
    debug = config['debug']
    info = {
        'last_ts': 0.0,
        'last_spotify_ts': 0,
        'add_last': False,
        'send_link': False,
        'spotify_tracks': [] # A list of spotify IDs to add, in order of latest to oldest
                             # Either a tuple of (type, id) or a slack timestamp to fetch
                             # Timestamps are stored if a !add is a reply
    }
    
    if debug:
        print("* Last action at {}".format(config['slack']['last_ts']))
    # Find any spotify links posted, and save the most recent.
    # Take only my messages and search for add commands

    # For each add command, if it's a reply, add the parent. If it's in the channel, add the last 
    # link posted. If it's naked (no reply and doesn't follow a spotify in this message load), add the 
    # last spotify recorded before now.

    add = False

    # If we're asked to post our currently playing link, take note of that
    send_link = False

    # Iterate the messages, look for !add and spotify links as thread parents
    mq = []
    for m in message_data:
        matches = track_regex.match(m['text'])

        if matches != None:
            if  info['last_spotify_ts'] == 0:
                info['last_spotify_ts'] = m['ts']
                if debug:
                    print("* Updating last spotify ts at {}".format(m['ts']))

            if add:
               if debug:
                   print("* Queuing tuple for add")
               mq.append((matches.group(1), matches.group(2)))
               add = False
        
            if 'thread_ts' in m.keys():
                if debug:
                    print("* Queuing thread {} for second pass".format(m['ts']))
                mq.append(m)

        if m['text'] == '!add' \
            and float(m['ts']) > config['slack']['last_ts'] \
            and m['user'] == config['slack']['user_id'] \
            and 'thread_ts' not in m.keys():
            if debug:
                print("* Got add command at ts {}".format(m['ts']))

            if info['last_ts'] < float(m['ts']):
                info['last_ts'] = float(m['ts'])

            add = True

        if m['text'] == '!link' and float(m['ts']) > config['slack']['last_ts'] and m['user'] == config['slack']['user_id'] and not send_link:
            if debug:
                print("* Got link command at {}".format(m['ts']))

            if info['last_ts'] < float(m['ts']):
                info['last_ts'] = float(m['ts'])

            send_link = True

    for m in mq:
        # This came from a top-level add, just pass it sraight in
        if isinstance(m,tuple):
            info['spotify_tracks'].append(m) 
            continue

        # Otherwise, check the thread info to see if I've replied with an add since the last ts.
        # If so, drop the timestamp of the thread into our return
        
        if config['slack']['user_id'] in m['reply_users'] \
            and float(m['latest_reply']) > config['slack']['last_ts']:
            thread = slack_client.get_replies(m['ts'])

            command = list(filter(lambda x: x['user'] == config['slack']['user_id']\
                                      and x['text'] == '!add'\
                                      and float(x['ts']) > config['slack']['last_ts'],\
                            thread))

            if len(command) > 0:
                info['spotify_tracks'].append(m['ts'])
                ts = float(command[-1]['ts'])
                if info['last_ts'] < ts:
                    info['last_ts'] = ts

    # Special case, naked !add. Flag it for the caller to fetch.
    info['add_last'] = add
    
    # Special case, we want to send the currently playing link
    info['send_link'] = send_link
    
    return info


    
@click.command()
@click.option('--channel', default=DEFAULT_CHANNEL, help='Slack channel to monitor')
@click.option('--debug', is_flag=True, help="Turn on debugging")
def main(channel, debug):
    global config
    global slack_client
    global spotify_client

    if not os.path.exists(CONFIG):
        first_run()
    else:
        config = util.load_config(CONFIG)

    config['debug'] = debug
    if config['debug']:
        print("* Startup config")
        print(config)

    global SLACK_CLIENT_ID, SLACK_SECRET, SPOTIFY_CLIENT_ID, SPOTIFY_SECRET
    SLACK_CLIENT_ID = config['slack_client']
    SLACK_SECRET = config['slack_secret']
    SPOTIFY_CLIENT_ID = config['spotify_client']
    SPOTIFY_SECRET = config['spotify_secret']

    if 'last_ts' in config['slack']:
        config['slack']['last_ts'] = float(config['slack']['last_ts'])
    else:
        config['slack']['last_ts'] = 0.0

    print ("* Config loaded")
    slack_client = SlackWrapper()
    spotify_client = SpotifyWrapper()

    if 'channel_id' not in config['slack'].keys() or config['slack']['channel_id'] == '':
        config['slack']['channel_id'] = slack_client.channel_id(channel)
        if config['debug']:
            print('* Saved channel id: {}'.format(config['slack']['channel_id']))
        util.write_config(config, CONFIG)

    # Watch message updates, tracking the times of last messages, saving the last spotify link in case we get a naked !add
    watch_messages()

if __name__ == "__main__":
    main()
