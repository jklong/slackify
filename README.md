1. `pip3 install <SLACKIFY WHEEL>`
2. `python3 -m slackify`

OR

1. `docker build -t slackify .`
2. `docker volume create slackify`
3. For the *first run*: `docker run --rm -it -e NAME=<YOUR MACHINE NAME> -v slackify:/data slackify:latest`
4. For all future runs: `docker run -d --name slackify -e NAME=<YOUR MACHINE NAME> -v slackify:/data slackify:latest`

OR

if you have k8s or whatever else, use that instead. thats up to you. 

We need to be interactive for the first run to grab API keys and stuff. The environment variable NAME forms part of a key. Of course docker breaks that with containers so if you don't specify it you'll lose your encrypted config file

The password is dicks