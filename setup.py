import setuptools

setuptools.setup(
    name = "slackify",
    version = "1.0.0",
    description = "Basic slack to spotify api glue",
    packages = setuptools.find_packages(),
    install_requires = ['click', 'cryptography', 'requests', 'slackclient']
)